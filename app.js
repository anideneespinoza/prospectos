const express = require("express")
const app = express()
const path = require('path')
const methodOverride = require("method-override")
const ejsMate = require("ejs-mate")
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, '/views'))
app.use(express.urlencoded({ extended: true }))
app.use(methodOverride('_method'))
app.engine("ejs",ejsMate)
const mongoose = require('mongoose')
const db = conectarBD()

const prospectoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    primerApellido: {
        type: String,
        required: true
    },
    segundoApellido: String,
    calle: {
        type: String,
        required: true
    },
    numero: Number,
    colonia: {
        type: String,
        required: true
    },
    codigoPostal: {
        type: Number,
        required: true
    },
    telefono: {
        type: Number,
        required: true
    },
    rfc: {
        type: String,
        unique: true,
        required: true
    },
    estado: {
        type: String,
        required: true
    },
    observaciones: {
        type: String
    }
})

const prospecto = mongoose.model('Prospecto', prospectoSchema)

app.listen(8080, () => {
    console.log("ESCUCHANDO EN PUERTO 8080")
})

app.get('/prospectos', async (req, res) => {
    const prospectos = await prospecto.find({})
    console.log(prospectos)
    res.render('home', { prospectos })
})
app.get('/prospectos/new', (req, res) => {
    res.render('new',{data:false})
})

app.get('/prospectos/:rfc', async(req, res) =>{
    const {rfc} = req.params
    const prospectos = await prospecto.find({rfc:rfc})
    res.render("detail",{prospecto: prospectos[0],valido: true})
})

app.patch("/prospectos/:rfc/aceptar",async(req,res)=>{
    const data = req.body
    const {rfc} = req.params
    await prospecto.updateOne({rfc:rfc},{estado: "Aceptado"})
    res.redirect("/prospectos")
})

app.patch("/prospectos/:rfc/rechazar",async(req,res)=>{
    const data = req.body
    const {rfc} = req.params
    if(data.observaciones.length>0){
        await prospecto.updateOne({rfc:rfc},{estado: "Rechazado", observaciones: data.observaciones})
        res.redirect("/prospectos")
    }
    else{
        const prospectos = await prospecto.find({rfc:rfc})
        res.render("detail",{prospecto:prospectos[0], valido: false})
    }
})

app.post('/prospectos', async(req, res) => {
    const data = req.body
    const valido = await verificar(data)
    if (valido === "OK") {
        const newProspecto = new prospecto({
            nombre: data.nombre,
            primerApellido: data.primerApellido,
            segundoApellido: data.segundoApellido,
            calle: data.calle,
            numero: data.numero,
            colonia: data.colonia,
            codigoPostal: data.codigoPostal,
            telefono: data.telefono,
            rfc: data.rfc,
            estado: "Enviado",
            observaciones: ""
        })
        console.log(data)
        await newProspecto.save()
        res.redirect("/prospectos")
    }
    else
    {
        console.log(data)
        console.log(valido)
        res.render("new",{data,valido})
    }
})

async function conectarBD() {
    return await mongoose.connect('mongodb://localhost:27017/Prospectos')
        .then(() => {
            console.log("CONECTADO")
        })
        .catch((error) => {
            console.log("NO SE PUDO CONECTAR CORRECTAMENTE: " + error)
        })
}

async function verificar(data) {
    const { nombre, primerApellido, segundoApellido, calle, numero, colonia, codigoPostal, telefono, rfc } = data
    if (nombre.length === 0 || primerApellido.length === 0 || calle.length === 0 || numero.length === 0 || colonia.length === 0 || codigoPostal.length === 0 || telefono.length === 0 || rfc.length === 0) {
        return "Ningún campo puede ir vacío"
    }
    if (rfc.length > 13) {
        return "RFC inválido"
    }
    const prospectos=await prospecto.find({rfc: rfc})
    console.log(prospectos.length)
    if(prospectos.length>0){
        return "Ya existe un cliente prospecto con ese RFC"
    }
    return "OK"
}